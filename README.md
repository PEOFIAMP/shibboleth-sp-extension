Shibboleth SP Extension for Privacy-Enhancing Protocols
===============================

# About this project

This is an open source implementation for Shibboleth SP
that achieves several privacy-enhancing protocols
proposed by the PEOFIAMP project.

For more information about PEOFIAMP, visit http://peofiamp.nii.ac.jp/

# License

The project is released under Apache license 2.0.

> Copyright 2013 The University of Tokyo
> Copyright 2013 Kyoto University
> 
> Licensed under the Apache License, Version 2.0 (the "License");
> you may not use this file except in compliance with the License.
> You may obtain a copy of the License at
> 
> http://www.apache.org/licenses/LICENSE-2.0
> 
> Unless required by applicable law or agreed to in writing, 
> software distributed under the License is distributed on 
> an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, 
> either express or implied.
> See the License for the specific language governing permissions 
> limitations under the License.

# How to Install

Please refer to INSTALL document.

