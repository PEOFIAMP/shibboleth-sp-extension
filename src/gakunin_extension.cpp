/*
 * Copyright 2013 The University of Tokyo
 * Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#if defined (_MSC_VER) || defined(__BORLANDC__)
# include "config_win32.h"
#else
# include "config.h"
#endif

#ifdef WIN32
# define _CRT_NONSTDC_NO_DEPRECATE 1
# define _CRT_SECURE_NO_DEPRECATE 1
# define MYEXT_EXPORTS __declspec(dllexport)
#else
# define MYEXT_EXPORTS
#endif

// TODO: probably too many of unused headers are here. Remove unnecessary ones :-P

#include <memory>

#include <shibsp/base.h>
#include <shibsp/exceptions.h>
#include <shibsp/Application.h>
#include <shibsp/SPConfig.h>
#include <shibsp/attribute/Attribute.h>
#include <shibsp/attribute/SimpleAttribute.h>
#include <shibsp/handler/AbstractHandler.h>
#include <shibsp/attribute/AttributeDecoder.h>
#include <xmltooling/logging.h>
#include <xmltooling/util/NDC.h>
#include <xmltooling/util/XMLHelper.h>
#include <xercesc/util/XMLUniDefs.hpp>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/iterator/indirect_iterator.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/tuple/tuple.hpp>
#include <saml/saml1/core/Assertions.h>
#include <saml/saml2/core/Assertions.h>

#include <sstream>

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <iomanip>
#include <openssl/dh.h>

#ifndef GAKUNIN_EXTENSION
#define GAKUNIN_EXTENSION "GakuninExtension"
#endif

#ifndef SHIBSP_LITE
# include <saml/SAMLConfig.h>
#endif
using namespace boost;
using namespace shibsp;
using namespace opensaml;
using namespace opensaml::saml2md;
using namespace xmltooling::logging;
using namespace xmltooling;
using namespace xercesc;
using namespace std;

namespace gakunin {
#ifndef SHIBSP_LITE
    AttributeExtractor* GakuninAttributeExtractorFactory(const DOMElement * const& e);
#endif

    Handler* SHIBSP_DLLLOCAL
    ExtraHandlerFactory(const pair<const DOMElement*,const char*>& p);
}
using namespace gakunin;

extern "C" int MYEXT_EXPORTS xmltooling_extension_init(void*)
{
    Category& log = Category::getInstance(GAKUNIN_EXTENSION);
    log.debug("xmltooling_extension_init()");

    SPConfig& conf = SPConfig::getConfig();
#ifndef SHIBSP_LITE
    log.debug("Registering GakuninAttributeExtractorFactory");
    // Non-LITE
    conf.AttributeExtractorManager.registerFactory("Gakunin", GakuninAttributeExtractorFactory);
#else
    // LITE
    log.debug("Registering ExtraHandlerFactory");
    conf.HandlerManager.registerFactory("Extra", ExtraHandlerFactory);
    log.debug("Finished registering.");
#endif
    return 0;   // signal success
}

extern "C" void MYEXT_EXPORTS xmltooling_extension_term()
{
    // Factories normally get unregistered during library shutdown,
    // so no work usually required here.
}
