/*
 *  Copyright 2013 The University of Tokyo
 *  Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <iomanip>
#include <sstream>

#include "utils.hpp"

using namespace std;

int gakunin::hex2dec(char c) {
    if ('0' <= c && c <= '9') {
        return (int)(c - '0');
    } else if ('A' <= c && c <= 'Z') {
        return (int)(c - 'A') + 10;
    } else if ('a' <= c && c <= 'z') {
        return (int)(c - 'a') + 10;
    }
    return -1;
}


