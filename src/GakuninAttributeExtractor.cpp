/*
 *  Copyright 2013 The University of Tokyo
 *  Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

# include "config.h"

// TODO: Remove unnecessary includes..
#include <shibsp/base.h>
#include <shibsp/exceptions.h>
#include <shibsp/Application.h>
#include <shibsp/SPConfig.h>
#include <shibsp/attribute/Attribute.h>
#include <shibsp/attribute/SimpleAttribute.h>
#include <shibsp/handler/AbstractHandler.h>
#include <shibsp/attribute/AttributeDecoder.h>
#include <shibsp/attribute/resolver/AttributeExtractor.h>
#include <xmltooling/logging.h>
#include <xmltooling/util/NDC.h>
#include <xmltooling/util/XMLHelper.h>
#include <xercesc/util/XMLUniDefs.hpp>

#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/iterator/indirect_iterator.hpp>
#include <boost/ptr_container/ptr_vector.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/scoped_ptr.hpp>
#include <boost/scoped_array.hpp>

#include <saml/saml1/core/Assertions.h>
#include <saml/saml2/core/Assertions.h>

#include <sstream>

#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <iomanip>
#include <openssl/bio.h>
#include <openssl/dh.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/x509.h>

#include "test.h"

#ifndef GAKUNIN_EXTENSION
#define GAKUNIN_EXTENSION "GakuninExtension"
#endif

// TODO: reduce the usage of "using namespace"..
using namespace boost;
using namespace shibsp;
using namespace opensaml;
using namespace opensaml::saml2md;
using namespace xmltooling::logging;
using namespace xmltooling;
using namespace xercesc;
using namespace std;

namespace gakunin {

    class SHIBSP_DLLLOCAL GakuninAttributeExtractor : public AttributeExtractor {
    private:
        Category& m_log;
        typedef map< pair<xstring,xstring>,pair< boost::shared_ptr<AttributeDecoder>,vector<string> > > attrmap_t;
        // TODO: Inconsistent naming.
        attrmap_t m_attrMap;

        /**
         * Investigate attributes and obtain relevant secret from it.
         * priv_key OR hex_secret will be available at this moment.
         *
         *  - priv_key is for Agent protocol.
         *  - hex_secret is for DH/Cascade.
         * 
         * TODO: introduce "Decrypter" abstract layer and unify
         * agent_priv_key_str and hex_secret.
         */
        bool obtainSecretFromAttributes(const vector< Attribute * > &attributes,
                                        auto_ptr<string>& agent_priv_key_str,
                                        auto_ptr<string>& hex_secret) const;

        // hex -> hex -> string
        string* decode(const string &encoded, const string &secret) const;
    public:
        GakuninAttributeExtractor(const DOMElement* e);
        virtual ~GakuninAttributeExtractor() {
        }

        void extractAttributes (const Application &application,
                                const RoleDescriptor *issuer,
                                const XMLObject &xmlObject,
                                vector< Attribute * > &attributes) const;

        void getAttributeIds (std::vector< std::string > &attributes) const;

        void generateMetadata (opensaml::saml2md::SPSSODescriptor &role) const;

        Lockable* lock() {
            return this;
        }

        void unlock() {
        }
    };

    AttributeExtractor* GakuninAttributeExtractorFactory(const DOMElement * const& e) {
        return new GakuninAttributeExtractor(e);
    }

    static const XMLCh _aliases[] =                 UNICODE_LITERAL_7(a,l,i,a,s,e,s);
    static const XMLCh _AttributeDecoder[] =        UNICODE_LITERAL_16(A,t,t,r,i,b,u,t,e,D,e,c,o,d,e,r);
    static const XMLCh _AttributeFilter[] =         UNICODE_LITERAL_15(A,t,t,r,i,b,u,t,e,F,i,l,t,e,r);
    static const XMLCh Attributes[] =               UNICODE_LITERAL_10(A,t,t,r,i,b,u,t,e,s);
    static const XMLCh _id[] =                      UNICODE_LITERAL_2(i,d);
    static const XMLCh isRequested[] =              UNICODE_LITERAL_11(i,s,R,e,q,u,e,s,t,e,d);
    static const XMLCh _MetadataProvider[] =        UNICODE_LITERAL_16(M,e,t,a,d,a,t,a,P,r,o,v,i,d,e,r);
    static const XMLCh metadataAttributeCaching[] = UNICODE_LITERAL_24(m,e,t,a,d,a,t,a,A,t,t,r,i,b,u,t,e,C,a,c,h,i,n,g);
    static const XMLCh metadataPolicyId[] =         UNICODE_LITERAL_16(m,e,t,a,d,a,t,a,P,o,l,i,c,y,I,d);
    static const XMLCh _name[] =                    UNICODE_LITERAL_4(n,a,m,e);
    static const XMLCh nameFormat[] =               UNICODE_LITERAL_10(n,a,m,e,F,o,r,m,a,t);
    static const XMLCh _TrustEngine[] =             UNICODE_LITERAL_11(T,r,u,s,t,E,n,g,i,n,e);
    static const XMLCh _type[] =                    UNICODE_LITERAL_4(t,y,p,e);

}  // namespace gakunin

using namespace gakunin;

namespace {
    int hex2dec(const char& c) {
        if ('0' <= c && c <= '9') {
            return (int)(c - '0');
        } else if ('A' <= c && c <= 'Z') {
            return (int)(c - 'A') + 10;
        } else if ('a' <= c && c <= 'z') {
            return (int)(c - 'a') + 10;
        }
        return -1;
    }

    /**
     * Base decoder.
     *
     * Here, decoder is responsible for decoding hex string to encrypted binary,
     * and decrypting the encrypted binary into original string.
     *
     * In other words, a given "encoded" string must be hex-encoded and
     * encrypted string given from IdP.
     */
    class DecoderBase {
    public:
        /**
         * A caller must take care of returned string.
         */
        virtual string* decode(const string& encoded) const = 0;
    };

    class SharedSecretDecoder : public DecoderBase {
    private:
        Category& m_log;
        scoped_ptr<string> m_secret;

        SharedSecretDecoder(string* secret);
    public:
        virtual ~SharedSecretDecoder() {}
        /*
         * This class will own a given "secret" object.
         */
        static SharedSecretDecoder* instanciate(string* secret);

        virtual string* decode(const string& encoded) const;
    };

    class AgentProtocolDecoder : public DecoderBase {
    private:
        Category& m_log;
        scoped_ptr<EVP_PKEY> m_pkey;
        AgentProtocolDecoder(EVP_PKEY* pkey);
    public:
        virtual ~AgentProtocolDecoder() {}
        static AgentProtocolDecoder* instanciate(string* agent_priv_key_str);

        virtual string* decode(const string& encoded) const;
    };

} // namespace

SharedSecretDecoder::SharedSecretDecoder(string* secret)
    : m_log(Category::getInstance(GAKUNIN_EXTENSION ".SharedSecretDecoder")),
      m_secret(secret) {
}

SharedSecretDecoder* SharedSecretDecoder::instanciate(string* secret) {
    if (secret) {
        return new SharedSecretDecoder(secret);
    } else {
        return NULL;
    }
}

string* SharedSecretDecoder::decode(const string& encoded) const {
    if (encoded.size() % 2 != 0) {
        m_log.error("encoded length is inappropriate (must be even number).");
        return NULL;
    }
    ostringstream oss;
    for (size_t i = 0; i < encoded.size(); i+=2) {
        int n1 = hex2dec(encoded.at(i));
        int n2 = hex2dec(encoded.at(i+1));

        int n3 = hex2dec(m_secret->at(i % m_secret->size()));
        int n4 = hex2dec(m_secret->at((i+1) % m_secret->size()));
        if (n1 < 0 || n2 < 0 || n3 < 0 || n4 < 0) {
            m_log.error("Unexpected input. encoded=\"%s\", m_secret=\"%s\", i=%d",
                        encoded.c_str(), m_secret->c_str(), i);
            return NULL;
        }
        int a = (n1 << 4) | n2;
        int b = (n3 << 4) | n4;
        char ch = (char)(a ^ b);
        oss << ch;
    }
    return new string(oss.str());
}

AgentProtocolDecoder::AgentProtocolDecoder(EVP_PKEY* pkey)
    : m_log(Category::getInstance(GAKUNIN_EXTENSION ".AgentProtocolDecoder")),
      m_pkey(pkey) {
}

AgentProtocolDecoder* AgentProtocolDecoder::instanciate(string* tmp) {
    // Should be freed in this scope.
    scoped_ptr<string> agent_priv_key_str(tmp);

    const int length = agent_priv_key_str->size();
    scoped_array<char> priv_key_mem(new char[length+1]);
    strcpy(priv_key_mem.get(), agent_priv_key_str->c_str());
    BIO *in = BIO_new_mem_buf(priv_key_mem.get(), -1);
    EVP_PKEY *pkey;
    if (NULL == test::agent_priv_key_password_cstr) {
      pkey = PEM_read_bio_PrivateKey(in, NULL, NULL, NULL);
    } else {
      const char* pass_phrase = test::agent_priv_key_password_cstr;
      scoped_array<char> copied_pass_phrase(new char[strlen(pass_phrase)+1]);
      strcpy(copied_pass_phrase.get(), pass_phrase);
      pkey = PEM_read_bio_PrivateKey(in, NULL, NULL, copied_pass_phrase.get());
    }
    BIO_free(in);
    if (pkey) {
        return new AgentProtocolDecoder(pkey);
    } else {
        return NULL;
    }
}

string* AgentProtocolDecoder::decode(const string& encoded) const {
    const size_t encoded_len = encoded.size();
    if (encoded_len % 2 != 0) {
        m_log.error("encoded length is inappropriate (must be even number).");
        return NULL;
    } else {
        const size_t encrypted_len = encoded_len / 2;
        scoped_array<unsigned char> encrypted(new unsigned char[encrypted_len]);
        bool success = true;
        for (size_t i = 0; i < encoded_len; i+=2) {
            int n1 = hex2dec(encoded[i]);
            int n2 = hex2dec(encoded[i+1]);
            if (n1 < 0 || n2 < 0) {
                m_log.error("@@@@@ Unexpected input. encoded=\"%s\",i=%d",
                            encoded.c_str(), i);
                success = false;
                break;
            } 
            encrypted[i/2] = (unsigned char)((n1 << 4) | n2);
        }
        if (!success) {
            return NULL;
        }
        m_log.debug("encrypted length: %u", encrypted_len);
        RSA* priv_rsa = m_pkey->pkey.rsa;
        int max_len = RSA_size(priv_rsa);
        scoped_array<unsigned char> decrypted(new unsigned char[max_len]);
        int decrypted_len = RSA_private_decrypt(encrypted_len, encrypted.get(),
                                                decrypted.get(),
                                                priv_rsa, RSA_PKCS1_PADDING);
        if (decrypted_len < 0) {
            int e = ERR_get_error();
            ERR_load_crypto_strings();
            m_log.error("Failed to decrypt the encrypted value: %s",
                        ERR_reason_error_string(e));
            ERR_free_strings();
        } else {
            decrypted[decrypted_len] = '\0';
            return new string((char *)decrypted.get());
        }
    }

    m_log.error("Failed to encode inside AgentProtocolDecoder");
    // Failed somewhere.
    return NULL;
}


GakuninAttributeExtractor::GakuninAttributeExtractor(const DOMElement* e)
    : m_log(Category::getInstance(GAKUNIN_EXTENSION ".AttributeExtractor")) {
    m_log.debug("(constructor)");

    // TODO: setup procidure should be done appropriately.
    return;

    if (!XMLHelper::isNodeNamed(e, shibspconstants::SHIB2ATTRIBUTEMAP_NS, Attributes)) {
        m_log.error("XML AttributeExtractor requires am:Attributes at root of configuration.");
        // throw ConfigurationException("XML AttributeExtractor requires am:Attributes at root of configuration.");
    }

    DOMElement* child = XMLHelper::getFirstChildElement(e, shibspconstants::SHIB2ATTRIBUTEMAP_NS, saml1::Attribute::LOCAL_NAME);
    m_log.debug("child's Tag: %s", child->getTagName());
    while (child) {
        // Check for missing name or id.
        const XMLCh* name = child->getAttributeNS(nullptr, _name);
        if (!name || !*name) {
            m_log.warn("skipping Attribute with no name");
            child = XMLHelper::getNextSiblingElement(child, shibspconstants::SHIB2ATTRIBUTEMAP_NS, saml1::Attribute::LOCAL_NAME);
            continue;
        }
        m_log.debug("name: %s", name);

        auto_ptr_char id(child->getAttributeNS(nullptr, _id));
        if (!id.get() || !*id.get()) {
            m_log.warn("skipping Attribute with no id");
            child = XMLHelper::getNextSiblingElement(child, shibspconstants::SHIB2ATTRIBUTEMAP_NS, saml1::Attribute::LOCAL_NAME);
            continue;
        }
        else if (!strcmp(id.get(), "REMOTE_USER")) {
            m_log.warn("skipping Attribute, id of REMOTE_USER is a reserved name");
            child = XMLHelper::getNextSiblingElement(child, shibspconstants::SHIB2ATTRIBUTEMAP_NS, saml1::Attribute::LOCAL_NAME);
            continue;
        }

        boost::shared_ptr<AttributeDecoder> decoder;
        try {
            DOMElement* dchild = XMLHelper::getFirstChildElement(child, shibspconstants::SHIB2ATTRIBUTEMAP_NS, _AttributeDecoder);
            if (dchild) {
                auto_ptr<xmltooling::QName> q(XMLHelper::getXSIType(dchild));
                if (q.get())
                    decoder.reset(SPConfig::getConfig().AttributeDecoderManager.newPlugin(*q.get(), dchild));
            }
            if (!decoder)
                decoder.reset(SPConfig::getConfig().AttributeDecoderManager.newPlugin(StringAttributeDecoderType, nullptr));
        }
        catch (std::exception& ex) {
            m_log.error("skipping Attribute (%s), error building AttributeDecoder: %s", id.get(), ex.what());
        }

        if (!decoder) {
            m_log.warn("No decoder is available.");
            child = XMLHelper::getNextSiblingElement(child, shibspconstants::SHIB2ATTRIBUTEMAP_NS, saml1::Attribute::LOCAL_NAME);
            continue;
        }

        // Empty NameFormat implies the usual Shib URI naming defaults.
        const XMLCh* format = child->getAttributeNS(nullptr, nameFormat);
        if (!format || XMLString::equals(format, shibspconstants::SHIB1_ATTRIBUTE_NAMESPACE_URI) ||
            XMLString::equals(format, saml2::Attribute::URI_REFERENCE)) {
            format = &chNull;  // ignore default Format/Namespace values
        }

        // Fetch/create the map entry and see if it's a duplicate rule.
        pair< boost::shared_ptr<AttributeDecoder>,vector<string> >& decl = m_attrMap[pair<xstring,xstring>(name,format)];
        if (decl.first) {
            m_log.warn("skipping duplicate Attribute mapping (same name and nameFormat)");
            child = XMLHelper::getNextSiblingElement(child, shibspconstants::SHIB2ATTRIBUTEMAP_NS, saml1::Attribute::LOCAL_NAME);
            continue;
        }

        if (m_log.isDebugEnabled()) {
            auto_ptr_char n(name);
            auto_ptr_char f(format);
            m_log.debug("Attribute %s%s%s", n.get(), *f.get() ? ", Format/Namespace:" : "", f.get());
        }

        decl.first = decoder;
        decl.second.push_back(id.get());

        // TODO: m_attributeIds? See also GSSAPIAttributeExtractor
        // TODO: alias?

        child = XMLHelper::getNextSiblingElement(child, shibspconstants::SHIB2ATTRIBUTEMAP_NS, saml1::Attribute::LOCAL_NAME);
    }
}

bool GakuninAttributeExtractor::obtainSecretFromAttributes(const vector< Attribute * > &attributes,
                                                           auto_ptr<string>& agent_priv_key_str,
                                                           auto_ptr<string>& hex_secret) const {
    // Note:
    // This function is inevitably with const because of
    // the caller function extractAttributes().

    auto_ptr<string> dh_sp_encrypted_priv_key;
    auto_ptr<string> dh_g;
    auto_ptr<string> dh_p;
    auto_ptr<string> dh_sp_pub_key;
    auto_ptr<string> dh_idp_pub_key;
    auto_ptr<string> cascade_sp_encrypted_k1;
    auto_ptr<string> secret;

    auto_ptr<string> agent_priv_key;

    bool dh_params_available = false;
    bool cascade_params_available = false;
    bool agent_params_available = false;

    // First look through attributes and lookup relevant decrypters.
    for (vector<Attribute*>::const_iterator it = attributes.begin();
         it != attributes.end();
         ++it) {
        Attribute *attr = *it;
        if (!strcmp(attr->getId(), "dh-p")) {
            dh_p.reset(new string(*(attr->getSerializedValues().begin())));
            dh_params_available = true;
        } else if (!strcmp(attr->getId(), "dh-g")) {
            dh_g.reset(new string(*(attr->getSerializedValues().begin())));
            dh_params_available = true;
        } else if (!strcmp(attr->getId(), "dh-idp-pub-key")) {
            dh_idp_pub_key.reset(new string(*(attr->getSerializedValues().begin())));
            dh_params_available = true;
        } else if (!strcmp(attr->getId(), "dh-sp-pub-key")
                   && attr->valueCount() > 0) {
            dh_sp_pub_key.reset(new string(*(attr->getSerializedValues().begin())));
            dh_params_available = true;
        } else if (!strcmp(attr->getId(), "dh-sp-encrypted-priv-key")
                   && attr->valueCount() > 0) {
            dh_sp_encrypted_priv_key.reset(new string(*(attr->getSerializedValues().begin())));
            dh_params_available = true;
        } else if (!strcmp(attr->getId(), "cascade-sp-encrypted-k1")) {
            cascade_sp_encrypted_k1.reset(new string(*(attr->getSerializedValues().begin())));
            cascade_params_available = true;
        } else if (!strcmp(attr->getId(), "ag-priv-key")) {
            agent_priv_key.reset(new string(*(attr->getSerializedValues().begin())));
            agent_params_available = true;

            // TODO: too long key may cause a serious problem, in which we fail to obtain
            // the value from attr. Need to investigate why it happens and how we can avoid it.
            //
            // Note that shibd itself successfully retrieves its content in Shibboleth.sso
            // interface, so it would be a bug on this side.
            m_log.debug("ag-priv-key found: %s", agent_priv_key->c_str());
        }
    }

    size_t count = ((dh_params_available ? 1 : 0)
                    + (cascade_params_available ? 1 : 0)
                    + (agent_params_available ? 1 : 0));
    if (count > 1) {
        m_log.warn("Params for multiple protocols are available.");
    }

    // variables used in several protocol handlings.
    string sp_secret_key_str(test::sp_secret_key_cstr);
    
    if (agent_params_available) {
        m_log.debug("Agent params detected.");

        if (agent_priv_key.get()) {
            agent_priv_key_str.reset(agent_priv_key.release());
            return true;
        } else {
            m_log.error("Encrypted/Pkcs7 is not implemented");
        }
        return false;
    } else if (cascade_params_available) {
        // CASCADE
        hex_secret.reset(decode(*cascade_sp_encrypted_k1, sp_secret_key_str));
        return true;
    } else if (dh_params_available) {
        // Diffie-Hellman
        if (!dh_p.get() || dh_p->size() == 0) {
            m_log.error("No DH-param P obtained. Exitting.");
            return false;
        }
        if (!dh_g.get() || dh_g->size() == 0) {
            m_log.error("No DH-param G obtained. Exitting.");
            return false;
        }
        if (!dh_idp_pub_key.get() || dh_idp_pub_key->size() == 0) {
            m_log.error("No IdP pub_key obtained. Exitting.");
            return false;
        }
        if (!dh_sp_pub_key.get() || dh_sp_pub_key->size() == 0) {
            m_log.error("No SP pub_key obtained. Exitting.");
            return false;
        }
        if (!dh_sp_encrypted_priv_key.get() || dh_sp_encrypted_priv_key->size() == 0) {
            m_log.error("No encrypted SP priv_key obtained. Exitting.");
            return false;
        }

        auto_ptr<string> dh_sp_priv_key(decode(*dh_sp_encrypted_priv_key, sp_secret_key_str));

        DH* dh = DH_new();

        if (!dh) {
            m_log.error("dh is null");
            return NULL;
        }

        BN_hex2bn(&dh->p, dh_p->c_str());
        BN_hex2bn(&dh->g, dh_g->c_str());
        BN_hex2bn(&dh->priv_key, dh_sp_priv_key->c_str());
        BN_hex2bn(&dh->pub_key, dh_sp_pub_key->c_str());

        BIGNUM* idp_pub_key = BN_new();
        BN_init(idp_pub_key);
        BN_hex2bn(&idp_pub_key, dh_idp_pub_key->c_str());

        {
            unsigned char* secret_cstr = new unsigned char[DH_size(dh)];
            int ret = DH_compute_key(secret_cstr, idp_pub_key, dh);
            if (ret < 0) {
                m_log.error("DH_compute_key() failed. ret=%d", ret);
                delete[] secret_cstr;

                BN_clear_free(idp_pub_key);
                DH_free(dh);
                return false;
            }

            ostringstream ss;
            ss << hex << setfill( '0' );
            for (int i = 0; i < ret; i++) {
                int c = (int)secret_cstr[i];
                ss << std::setw( 2 ) << c;
            }

            // TODO: is this appropriate enough?
            hex_secret.reset(new string(ss.str()));
            delete[] secret_cstr;
        }

        string p, g, pub_key, priv_key;
        {
            char *p_tmp = BN_bn2hex(dh->p);
            char *g_tmp = BN_bn2hex(dh->g);
            char *pub_key_tmp = BN_bn2hex(dh->pub_key);
            char *priv_key_tmp = BN_bn2hex(dh->priv_key);
            p = string(p_tmp);
            g = string(g_tmp);
            pub_key = string(pub_key_tmp);
            priv_key = string(priv_key_tmp);
            boost::algorithm::to_lower(p);
            boost::algorithm::to_lower(g);
            boost::algorithm::to_lower(pub_key);
            boost::algorithm::to_lower(priv_key);
            OPENSSL_free(p_tmp);
            OPENSSL_free(g_tmp);
            OPENSSL_free(pub_key_tmp);
            OPENSSL_free(priv_key_tmp);
        }

        m_log.debug("sp_secret_key_str=%s", sp_secret_key_str.c_str());
        m_log.debug("sp_encrypted_priv_key=%s", dh_sp_encrypted_priv_key->c_str());
        m_log.debug("p=%s", p.c_str());
        m_log.debug("g=%s", g.c_str());
        m_log.debug("idp_pub=%s", dh_idp_pub_key->c_str());
        m_log.debug("sp_pub =%s", dh_sp_pub_key->c_str());
        m_log.debug("sp_priv=%s", dh_sp_priv_key->c_str());

        // DHparams_print_fp(stdout, dh);

        // TODO; should be able to wrap some auto_ptr thing..
        BN_clear_free(idp_pub_key);
        DH_free(dh);
        return true;
    }

    return false;
}

void GakuninAttributeExtractor::getAttributeIds (std::vector< std::string > &attributes) const {
    m_log.debug("getAttributeIds(). current attribute id count: %d", attributes.size());
}

void GakuninAttributeExtractor::generateMetadata (opensaml::saml2md::SPSSODescriptor &role) const {
    m_log.debug("generateMetadata()");
}

string* GakuninAttributeExtractor::decode(const string &encoded,
                                          const string &hex_secret) const {
    if (encoded.size() % 2 != 0) {
        m_log.error("encoded length is inappropriate (must be even number).");
        return NULL;
    }
    ostringstream oss;
    for (size_t i = 0; i < encoded.size(); i+=2) {
        int n1 = hex2dec(encoded.at(i));
        int n2 = hex2dec(encoded.at(i+1));

        int n3 = hex2dec(hex_secret.at(i % hex_secret.size()));
        int n4 = hex2dec(hex_secret.at((i+1) % hex_secret.size()));
        if (n1 < 0 || n2 < 0 || n3 < 0 || n4 < 0) {
            m_log.error("Unexpected input. encoded=\"%s\", hex_secret=\"%s\", i=%d",
                        encoded.c_str(), hex_secret.c_str(), i);
            return NULL;
        }
        int a = (n1 << 4) | n2;
        int b = (n3 << 4) | n4;
        char ch = (char)(a ^ b);
        oss << ch;
    }
    return new string(oss.str());
}

void GakuninAttributeExtractor::extractAttributes (const Application &application,
                                                   const RoleDescriptor *issuer,
                                                   const XMLObject &xmlObject,
                                                   vector< Attribute * > &attributes) const {
    if (attributes.size() == 0) {
        m_log.debug("extractAttributes() skipped (no attributes to process)");
        return;
    }
    m_log.debug("extractAttributes(). current attributes: %d", attributes.size());

    scoped_ptr<DecoderBase> decoder;

    {
        auto_ptr<string> agent_priv_key_str;
        auto_ptr<string> hex_secret;
        if (!obtainSecretFromAttributes(attributes, agent_priv_key_str, hex_secret)) {
            m_log.error("Failed to obtain priv_key or hex_secret.");
            return;
        }

        if (agent_priv_key_str.get() != NULL) {
            m_log.debug("Private key will be used (Agent).");
            decoder.reset(AgentProtocolDecoder::instanciate(agent_priv_key_str.release()));
            if (NULL == decoder.get()) {
              m_log.error("Failed to obtain a decoder object using the given private key.");
            }
        } else if (hex_secret.get() != NULL) {
            m_log.debug("hex_secret=%s", hex_secret->c_str());
            decoder.reset(SharedSecretDecoder::instanciate(hex_secret.release()));
        } else {
            m_log.error("Inconsistent state. "
                        "Both priv_key and hex_secret are not available "
                        "while obtaining function returned successfully.");
            return;
        }
    }
    

    auto_ptr< vector<Attribute *> > holder(new vector<Attribute *>());
    for (vector<Attribute*>::iterator it = attributes.begin();
         it != attributes.end();
         ++it) {
        Attribute *attr = *it;
        // If an attribute is one that is for decode/decrypt other
        // attributes, we skip them.
        //
        // TODO: should check their namespaces.
        // TODO: read these from somewhere else?
        if (!strcmp(attr->getId(), "dh-p") ||
            !strcmp(attr->getId(), "dh-g") ||
            !strcmp(attr->getId(), "dh-idp-pub-key") ||
            !strcmp(attr->getId(), "dh-sp-encrypted-priv-key") ||
            !strcmp(attr->getId(), "dh-sp-pub-key") ||
            !strcmp(attr->getId(), "cascade-sp-encrypted-k1") ||
            !strcmp(attr->getId(), "ag-priv-key")) {
            continue;
        }
        if (attr->valueCount() == 0) {
            m_log.debug("id=\"%s\", value=(none)", attr->getId());
            continue;
        }

        {
            // debug.
            size_t count = attr->valueCount();
            stringstream oss;
            for (size_t i = 0; i < count; i++) {
                if (i != 0) {
                    oss << ", ";
                }
                oss << attr->getString(i);
            }
            m_log.debug("id=\"%s\", value=\"%s\"", attr->getId(), oss.str().c_str());
        }

        SimpleAttribute* simple_attr = dynamic_cast<SimpleAttribute*>(attr);
        if (simple_attr != NULL) {
            vector<string>& values = simple_attr->getValues();
            for (vector<string>::iterator values_it = values.begin();
                 values_it != values.end();
                 ++values_it) {

                auto_ptr<string> decoded;
                if (decoder.get()) {
                    decoded.reset(decoder->decode(*values_it));
                }
                if (decoded.get()) {
                    m_log.debug("Adding decoded value \"%s\"", decoded->c_str());
                    *values_it = *decoded;
                } else {
                    m_log.warn("Failed to decode \"%s\".", values_it->c_str());
                }
            }
        } else {
            // TODO: Check if Attribute other than SimpleAttribute may come here.
            // e.g. ScopedAttribute, NameIDAttribute, ExtensibleAttribute,
            // XMLAttribute.
            //
            // Conclusion: ScopedAttribute may come here (it actually happened when
            // dealing with agent protocol type while it didn't with dh/cascade).
            m_log.error("Unimplemented Attribute type for %s: \"%s\"",
                        attr->getId(), typeid(*attr).name());
        }
    }
}
