/*
 *  Copyright 2013 The University of Tokyo
 *  Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <boost/algorithm/string.hpp>
#include <boost/shared_ptr.hpp>
#include <iomanip>
#include <cstdlib>
#include <ctime>
#include <vector>

// Needed in Debian 7.0 (not in Ubuntu 12.04 LTS),
// to suppress strange error messages in shibboleth
// headers. Don't know exactly why.
#include <xmltooling/unicode.h>

#include <shibsp/base.h>
#include <shibsp/handler/AbstractHandler.h>
#include <shibsp/handler/RemotedHandler.h>

#include <xmltooling/logging.h>
#include <xmltooling/util/NDC.h>
#include <xmltooling/util/XMLHelper.h>
#include <xercesc/util/XMLUniDefs.hpp>

#include <openssl/crypto.h>
#include <openssl/dh.h>
#include <openssl/err.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>

#ifndef GAKUNIN_EXTENSION
#define GAKUNIN_EXTENSION "GakuninExtension"
#endif

#include "utils.hpp"
#include "test.h"

using namespace xmltooling;
using namespace xercesc;
using namespace shibsp;
using namespace std;

int dbg_hex2dec(char c) {
    if ('0' <= c && c <= '9') {
        return (int)(c - '0');
    } else if ('A' <= c && c <= 'Z') {
        return (int)(c - 'A') + 10;
    } else if ('a' <= c && c <= 'z') {
        return (int)(c - 'a') + 10;
    }
    return -1;
}

template<typename T>
string* dbg_encode(const T& raw, const string& hex_secret) {
    if (hex_secret.size() % 2 != 0) {
        // m_log.error("hex_secret length is inappropriate (must be even).");
        return NULL;
    }
    ostringstream ss;
    ss << hex << setfill('0');
    for (size_t i = 0; i < raw.size(); ++i) {
        char ch = raw.at(i);
        int a = (int)ch;
        size_t j = (i*2);
        int n1 = dbg_hex2dec(hex_secret.at(j % hex_secret.size()));
        int n2 = dbg_hex2dec(hex_secret.at((j+1) % hex_secret.size()));
        if (n1 < 0 || n2 < 0) {
            // mlog
            return NULL;
        }
        int b = (n1 << 4) | n2;
        int c = (a ^ b) & 0xff;
        ss << std::setw( 2 ) << c;
    }
    return new string(ss.str());
}


namespace gakunin {
    class SHIBSP_DLLLOCAL ExtraHandler : public AbstractHandler {
    public:
        ExtraHandler(const DOMElement* e, const char* appId);
        virtual ~ExtraHandler() {}

        std::pair<bool,long> run(SPRequest& request, bool isHandler=true) const;
    private:
        std::pair<bool,long> error_out(SPRequest& request, const string& err_message) const;
        // string* encode(const string& raw, const string& hex_secret) const;

        /**
         * Generate a random byte sequence. It does NOT include terminating NULL.
         * Not human-readable, either.
         *
         * The caller must be responsible for freeing the pointer (using delete[]).
         */
        vector<char>* generate_random_sequence(const size_t length) const;
        bool get_dh_params(auto_ptr<string>& p,
                           auto_ptr<string>& q,
                           auto_ptr<string>& pub_key,
                           auto_ptr<string>& priv_key) const;
        bool get_cascade_params(const string& sp_secret_key_str,
                                auto_ptr<string>& pxi_k1,
                                auto_ptr<string>& sp_k1) const;

        /**
         * The caller must be responsible for freeing the pointer
         * (using EVP_PKEY_free()
         */
        EVP_PKEY *obtain_pubkey(const char* cert_path) const;
    };

    Handler* SHIBSP_DLLLOCAL ExtraHandlerFactory(const pair<const DOMElement*,const char*>& p) {
        return new ExtraHandler(p.first, p.second);
    }
}
using namespace gakunin;

ExtraHandler::ExtraHandler(const DOMElement* e, const char* appId)
    : AbstractHandler(e, xmltooling::logging::Category::getInstance(GAKUNIN_EXTENSION ".ExtraHandler")) {
    m_log.debug("(ExtraHandler constructor): appId=%s", appId);

    srandom((unsigned int) time(NULL));

    m_log.debug("Prepare OpenSSL library.");

    SSL_library_init();
    OpenSSL_add_all_ciphers();
    OpenSSL_add_all_digests();
    OpenSSL_add_all_algorithms();

    m_log.debug("(ExtraHandler constructor): done");

}

vector<char>* ExtraHandler::generate_random_sequence(const size_t length) const {
    // TODO: need to be reviewed. Randomness is really difficult.
    size_t int_size = sizeof(int);
    auto_ptr< vector<char> > ret(new vector<char>(length));
    for (size_t i = 0; i < length;) {
        int r = random();
        for (size_t j = 0; j < int_size && i < length; j++, i++) {
            size_t shift = (int_size-j-1);
            (*ret)[i] = (r >> shift) & 0xff;
        }
    }
    return ret.release();
}

std::pair<bool,long> ExtraHandler::error_out(SPRequest& request, const string& err_message) const {
    stringstream s;
    s << "<html><head><title>Result</title></head><body>" << endl;
    s << "<pre>" << err_message << "</pre>" << endl;
    s << "</body></html>" << endl;
    request.setContentType("text/html");
    request.setResponseHeader("Expires","01-Jan-1997 12:00:00 GMT");
    request.setResponseHeader("Cache-Control","private,no-store,no-cache");
    return make_pair(true, request.sendResponse(s));
}

bool ExtraHandler::get_dh_params(auto_ptr<string>& p,
                                 auto_ptr<string>& g,
                                 auto_ptr<string>& pub_key,
                                 auto_ptr<string>& priv_key) const {

    DH* dh_sp = DH_new();
    if (!DH_generate_parameters_ex(dh_sp, 512, DH_GENERATOR_5, 0)) {
        m_log.error("Failed to generate DH params.");
        DH_free(dh_sp);
        return false;
    }
    int codes = 0;
    if (!DH_check(dh_sp, &codes)) {
        char *p_cstr = BN_bn2hex(dh_sp->p);
        char *g_cstr = BN_bn2hex(dh_sp->g);
        m_log.error("DH_check() failed. p=\"%s\", g=\"%s\"",
                    p_cstr, g_cstr);
        OPENSSL_free(p_cstr);
        OPENSSL_free(g_cstr);
        DH_free(dh_sp);
        return false;
    }
    if (codes != 0) {
        stringstream err_ss;
        if (DH_CHECK_P_NOT_PRIME & codes) {
            err_ss << "P is not a prime.";
        }
        if (DH_CHECK_P_NOT_SAFE_PRIME & codes) {
            err_ss << "P is not a safe prime.";
        }
        if (DH_UNABLE_TO_CHECK_GENERATOR & codes) {
            err_ss << "Unable to check generator.";
        }
        if (DH_NOT_SUITABLE_GENERATOR & codes) {
            err_ss << "Not a suitable generator.";
        }
        DH_free(dh_sp);
        m_log.error("DH_check() returned non-0 codes: %s",
                    err_ss.str().c_str());
        return false;
    }

    if (!DH_generate_key(dh_sp)) {
        int e = ERR_get_error();
        ERR_load_crypto_strings();
        m_log.error("DH_generate_key() failed. %s",
                    ERR_reason_error_string(e));
        ERR_free_strings();
        DH_free(dh_sp);
        return false;
    }

    {
        char *p_tmp = BN_bn2hex(dh_sp->p);
        char *g_tmp = BN_bn2hex(dh_sp->g);
        char *pub_key_tmp = BN_bn2hex(dh_sp->pub_key);
        char *priv_key_tmp = BN_bn2hex(dh_sp->priv_key);
        p.reset(new string(p_tmp));
        g.reset(new string(g_tmp));
        pub_key.reset(new string(pub_key_tmp));
        priv_key.reset(new string(priv_key_tmp));
        boost::algorithm::to_lower(*p);
        boost::algorithm::to_lower(*g);
        boost::algorithm::to_lower(*pub_key);
        boost::algorithm::to_lower(*priv_key);
        OPENSSL_free(p_tmp);
        OPENSSL_free(g_tmp);
        OPENSSL_free(pub_key_tmp);
        OPENSSL_free(priv_key_tmp);
    }

    DH_free(dh_sp);
    return true;
}

EVP_PKEY* ExtraHandler::obtain_pubkey(const char* cert_path_cstr) const {
    EVP_PKEY* pub_key = NULL;

    m_log.debug("Tries to load %s as X509 certificate.", cert_path_cstr);
    
    FILE *cert_fp = fopen(cert_path_cstr, "r");
    if (cert_fp) {
        X509 *cert = PEM_read_X509(cert_fp, NULL, NULL, NULL);
        if (cert) {
            pub_key = (EVP_PKEY *) X509_get_pubkey(cert);
            X509_free(cert);
        }
        fclose(cert_fp);
    }

    if (!pub_key) {
        m_log.debug("Tries to load %s as misc pubkey.", cert_path_cstr);
        BIO *in = BIO_new_file(test::pxi_cert_path_cstr, "r");
        if (in) {
            pub_key = PEM_read_bio_PUBKEY(in, NULL,NULL, NULL);
            BIO_free(in);
        }
    }

    if (!pub_key) {
        m_log.error("Failed to load pub_key.");
    }
    return pub_key;
}

/**
 * Prepares params for Cascade.
 */
bool ExtraHandler::get_cascade_params(const string& sp_secret_key_str,
                                      auto_ptr<string>& pxi_k1,
                                      auto_ptr<string>& sp_k1) const {
    auto_ptr< vector<char> > cascade_k1(generate_random_sequence(test::k1_length));
    auto_ptr<string> encoded_k1(bin2hex(*cascade_k1));
    m_log.debug("k1=%s", encoded_k1->c_str());

    sp_k1.reset(gakunin::encode(*encoded_k1, sp_secret_key_str));

    EVP_PKEY * pub_key = obtain_pubkey(test::pxi_cert_path_cstr);
    if (!pub_key) {
        m_log.error("Failed to obtain pubkey (maybe the file \"%s\" is missing)",
                    test::pxi_cert_path_cstr);
        return false;
    }
    RSA* rsa = pub_key->pkey.rsa;
    if (!rsa) {
        m_log.error("Failed to get RSA from pub_key (path: \"%s\")",
                    test::pxi_cert_path_cstr);
        EVP_PKEY_free(pub_key);
        return false;
    }

    char* k1_array = &(*cascade_k1)[0];
    // size_t from_size = strlen(priv_key_cstr);
    int to_size = RSA_size(rsa);
    unsigned char* to = new unsigned char[to_size];
    int ret = RSA_public_encrypt(cascade_k1->size(),
                                 (unsigned char*)k1_array,
                                 to, rsa, RSA_PKCS1_PADDING);
    if (ret < 0) {
        int e = ERR_get_error();
        ERR_load_crypto_strings();
        // You may see the error saying "data too large for key size."
        // It means you need to prepare larger RSA key 
        m_log.error("Failed to encrypt K1 (length: %d) with RSA (%s)",
                    test::k1_length, ERR_reason_error_string(e));
        ERR_free_strings();

        delete[] to;
        EVP_PKEY_free(pub_key);
        return false;
    }

    // Encoding binary form of pxi_k1 to hex.
    ostringstream ss;
    ss << hex << setfill('0');
    for (int i = 0; i < ret; i++) {
        int c = (int)to[i];
        ss << std::setw( 2 ) << c;
    }
    pxi_k1.reset(new string(ss.str()));

    delete[] to;
    EVP_PKEY_free(pub_key);
    return true;
}

std::pair<bool,long> ExtraHandler::run(SPRequest& request, bool isHandler) const {
    m_log.debug("run() start");

    // TODO: this interface will soon become a target of DOS attack.
    // need to obscure the URL with one-time password or something.

    HTTPResponse& response = request;
    // Prepare DH params.
    auto_ptr<string> p, g, pub_key, priv_key;
    if (!get_dh_params(p, g, pub_key, priv_key)) {
        return error_out(request, "Failed to generate DH params.");
    }
    m_log.debug("p=%s", p->c_str());
    m_log.debug("g=%s", g->c_str());
    m_log.debug("sp_pub_key=%s", pub_key->c_str());
    m_log.debug("sp_priv_key=%s", priv_key->c_str());
    string sp_secret_key_str(test::sp_secret_key_cstr);
    auto_ptr<string> encrypted_priv_key(dbg_encode(*priv_key, sp_secret_key_str));

    auto_ptr<string> pxi_k1, sp_k1;
    if (!get_cascade_params(sp_secret_key_str, pxi_k1, sp_k1)) {
        return error_out(request, "Failed to generate Cascade params.");
    }

    m_log.debug("pxi_k1=%s", pxi_k1->c_str());
    m_log.debug("sp_k1=%s", sp_k1->c_str());

    stringstream ss;
    // TODO: determine appropriate protocol.
    ss << "<gakunin>" << endl;
    ss << "  <dh>" << endl
       << "    <dh_p>" << *p << "</dh_p>" << endl
       << "    <dh_g>" << *g << "</dh_g>" << endl
       << "    <dh_sp_pub_key>" << *pub_key << "</dh_sp_pub_key>" << endl
       << "    <dh_sp_encrypted_priv_key>" << *encrypted_priv_key
       << "</dh_sp_encrypted_priv_key>" << endl
       << "  </dh>" << endl;
    ss << "  <cascade>" << endl
       << "    <cascade_pxi_k1>" << *pxi_k1 << "</cascade_pxi_k1>" << endl
       << "    <cascade_sp_k1>" << *sp_k1 << "</cascade_sp_k1>" << endl
       << "  </cascade>" << endl;
    ss << "</gakunin>" << endl;

    // TODO: RSA_blinding_on()
    // TODO: RSA_blinding_off()

    request.setContentType("text/xml");
    m_log.debug("run() end.");
    return make_pair(true, response.sendResponse(ss));
}

