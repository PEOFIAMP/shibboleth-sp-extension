/*
 *  Copyright 2013 The University of Tokyo
 *  Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

namespace test {
    /**
     * Secret key just available inside SP, which
     * encodes/decodes every secret information only
     * SP can interpret.
     *
     * Note:
     * RSA encryption may fail if this string is too long.
     * (More in general RSA encryption isn't suitable enough
     * to encrypt a very long string).
     */

    // Too long string may be refused by RSA encryption
    // (The exact size will differ depending on the key length)
    static const size_t k1_length = 64;

    /*
     * TODO: should prepare more sophisticated SP secret with
     * better security mechanism (symmetric encryption).
     */
    static const char* sp_secret_key_cstr = "t3slsll6xiev9pb4sbzochjyt66qjtnzbify4gx3hibejdr7y8zacqv1ehuf0745f569m4bhlyb3wqohcg3wsx6jj5mnupzm4n3uy1u40jg1i4x3atl333j02gbvym3maocke3405wjiexqnyzy1orqrdw7m3by7y02nm7klc45ahl4fct5gxte13iw9dyikf592g7adhqowqz70hy426v9knvya20ne60ux9kyxbmkafwgbu4sgnm89j2azjt3ey42oz9eygicsaly6eun56y2pfra9lwrbrs20qrbtyv28h9xvi0mlsbc15qbtrar3ssozrdpn5q9dwfu7egnouhok9rwgblo80fmej5ipiyofuin9vn1m9s0mlg5bh3om4iyg42azg4ih3gv6tfiia4i9sut0xr5g9hx7dmbq4725gs6tugovpzk3fhcm6e85yuvl9kzxhg3bujz86jobwnjtgqly59xs9u0tuep32rp275mfrmbagr4m2j6kdk5l68lcji57w56shr5wiwi1a2oltd7wcfc2795xkqjwri2ap2s3ppe6ex6y136a5aukgarx70qgnoznstmv8edclcomkdrecl1d7h19fjih5pqef7txivfq357prc5ehjttreuhsvfmb4adu62ewrj2p8kn4y1ew9ydrh50rk0xzv2pd2ysbqn5yrdfwnmtp8c3as9wr1gmdkxb8k81o7cs5jkqeo9510v2szr0zczjollfvxbno8l85ymy6b5aeyxk7ws95dw9kqabkrmpeaui41ln0blm5hdl874j9hmpr93n8umk2y2w74an1gomzeruwuas81ielqqaciqothn4svmhbsazd8h3hg3o9oqxr4lor6d5u89uy4y5h9sw44vow5jb86u5faabbj0t8oq2jvs1hvg1rmdt1pzsrcespv4rlds89i39t770kej3lq23iph7vqar538en4qf56w7spuc3hdkfpjtggv0ts3ntafu7u68s5mnncrp4lbryim9";

    /**
     * Path for PxI's certificate.
     * It will be used to encrypt cascade's K1.
     */
    static const char* pxi_cert_path_cstr = "/usr/local/ssl/pxi.crt";

    /**
     * A password for Agent protocol's private key.
     * Can be NULL if no password is provided;
     */
    static const char* agent_priv_key_password_cstr = "VSd6YvVL2m";
}

