/*
 *  Copyright 2013 The University of Tokyo
 *  Copyright 2013 Kyoto University
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>
#include <iomanip>
#include <sstream>
#include <vector>

namespace gakunin {
    int hex2dec(char c);

    template<typename T> std::string *bin2hex(const T& raw) {
        std::ostringstream ss;
        ss << std::hex << std::setfill('0');
        for (size_t i = 0; i < raw.size(); ++i) {
            int c = ((int) raw.at(i)) & 0xff;
            ss << std::setw( 2 ) << c;
        }
        return new std::string(ss.str());
    }

    template<typename T> std::string* encode(const T& raw,
                                             const std::string& hex_secret) {
        if (hex_secret.size() % 2 != 0) {
            // m_log.error("hex_secret length is inappropriate (must be even).");
            return NULL;
        }
        std::ostringstream ss;
        ss << std::hex << std::setfill('0');
        for (size_t i = 0; i < raw.size(); ++i) {
            char ch = raw.at(i);
            int a = (int)ch;
            size_t j = (i*2);
            int n1 = hex2dec(hex_secret.at(j % hex_secret.size()));
            int n2 = hex2dec(hex_secret.at((j+1) % hex_secret.size()));
            if (n1 < 0 || n2 < 0) {
                // mlog
                return NULL;
            }
            int b = (n1 << 4) | n2;
            int c = (a ^ b) & 0xff;
            ss << std::setw( 2 ) << c;
        }
        return new std::string(ss.str());
    }
}
